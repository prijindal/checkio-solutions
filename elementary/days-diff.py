import datetime

def days_diff(date1, date2):
    D1 = datetime.datetime(date1[0], date1[1],date1[2])
    D2 = datetime.datetime(date2[0], date2[1],date2[2])
    diff = D1 - D2
    return abs(diff.days)

print(days_diff((1982, 4, 19), (1982, 4, 22)))
