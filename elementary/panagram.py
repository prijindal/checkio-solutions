def check_panagram(astring):
    astring = astring.lower()
    CHARS = 'abcdefghijklmnopqrstuvwxyz'
    for i in CHARS:
        try:
            astring.index(i)
        except ValueError:
            return False
    return True

print(check_panagram("The quick "))
