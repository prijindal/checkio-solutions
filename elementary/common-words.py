def checkio(list1, list2):
    L = set(list1.split(','))
    R = set(list2.split(','))
    I = sorted(list(L.intersection(R)))
    return ','.join(I)

checkio("hello,world", "hello,earth")
checkio("one,two,three", "four,five,six")
checkio("one,two,three", "four,five,one,two,six,three")
