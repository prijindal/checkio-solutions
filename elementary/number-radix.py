def check_valid(num, base):
    CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for i in num:
        if CHARS.index(i) > base-1:
            return False
    return True

def checkio(num, base):
    CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    if check_valid(num, base):
        new = 0
        k = 0
        n = len(num) - 1
        for i in num:
            new+=CHARS.index(i)*(base**(n - k))
            k+=1
        return new
    else:
        return -1

checkio("AF", 16);
checkio("101", 2)
checkio("101", 5)
checkio("Z", 36)
checkio("AB", 10)
