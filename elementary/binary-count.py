def str_to_bin(num):
    if num//2==0:
        return str(num%2)
    else :
        return str_to_bin(num//2) + str(num%2)

def binary_count(num):
    A = str_to_bin(num)
    count = 0
    for i in A:
        if i =='1':
            count+=1
    return count
print(binary_count(10))
