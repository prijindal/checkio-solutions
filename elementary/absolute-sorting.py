def checkio(aset):
    aset = list(aset)
    for i in range(len(aset)):
        for j in range(i, len(aset)):
            if abs(aset[i]) > abs(aset[j]):
                aset[i], aset[j] = aset[j], aset[i]
    print(aset)

checkio((-20, -5, 10, 15))
checkio((1, 2, 3, 0))
checkio((-1, -2, -3, 0))
